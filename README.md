<div align="center">
    <img src="https://gitlab.com/grzgajda/pagespeed/raw/master/logo.png" alt="pagespeed" />
</div>

<div align="center">
    <strong>Simple CLI application written in PHP to benchmark response time one or more websites and compare results to first input.</strong>
    <br />
    <br />
</div>

<div align="center">
  <img src="https://img.shields.io/badge/LANG-PHP-%234F5D95.svg?style=for-the-badge" />
  <img src="https://img.shields.io/gitlab/pipeline/grzgajda/pagespeed.svg?logo=gitlab&style=for-the-badge" />
  <img src="https://img.shields.io/badge/coverage-69.35%25-brightgreen.svg?logo=gitlab&style=for-the-badge" />
</div>

<br />
<br />

I wrote this application to show nice (I think) usage of generators in PHP where we can control when to stop making next requests (e.g. when we find the first website faster than our). Point is to not store the whole data in application memory but each time processing data until next request.

## Installation

To run this application, first you need to install dependencies. The simplest (and perhaps the only) way is to use _composer_ packager.

```bash
composer install
```

## Usage

_PageSpeed_ is simple CLI application with PHP executable file _pagespeed_ in main directory. As arguments you need to pass a list of URLs to test where the first one (known as `main_url`) is the one to compare with another websites.

<div align="center">
    <img src="https://gitlab.com/grzgajda/pagespeed/raw/master/terminal.png" alt="pagespeed" />
</div>

By default, command makes output to CLI with one pretty table with five values: main url, tested url, is main url faster than tested one, duration time for main and tested request. After benchmark, application prints to console the output:

<div align="center">
    <img src="https://gitlab.com/grzgajda/pagespeed/raw/master/terminal_output.png" alt="pagespeed" />
</div>

## Composability of actions

The core of application is class `\GGajda\PageSpeed\Benchmark\PageSpeed` which lives independently
of command and actions. To increase usage of application, there is possibility to add _actions_.

```php
public function __construct(BenchmarkRunner $pageSpeed, CommandResult ...$actions)
```

Each action needs to implements interface `GGajda\PageSpeed\ResultAction\CommandResult`:

```php
interface CommandResult
{
    public function beforeCommand(InputInterface $input, OutputInterface $output): void;

    public function handleResult(TestResult $result, InputInterface $input, OutputInterface $output): void;

    public function afterCommand(InputInterface $input, OutputInterface $output): void;
}
```

As you can see, _actions_ are created only for console commands as they're are not a part of benchmarks. 
