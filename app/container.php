<?php declare (strict_types = 1);

use GGajda\PageSpeed\Benchmark;
use GGajda\PageSpeed\Command;
use GGajda\PageSpeed\ResultAction;
use Psr\Container\ContainerInterface;

return [
    Benchmark\PageSpeed::class => function (ContainerInterface $container): Benchmark\PageSpeed {
        return (new Benchmark\Factory\HttpPageSpeedFactory($container))->create();
    },
    Benchmark\TestRunner::class => \DI\get(Benchmark\Http\HttpTestRunner::class),
    Benchmark\Http\HttpTestRunner::class => function (ContainerInterface $container): Benchmark\TestRunner {
        return (new Benchmark\Factory\HttpTestRunnerFactory($container))->create();
    },
    Command\PageSpeed::class => function (ContainerInterface $container): Command\PageSpeed {
        return (new Command\PageSpeedFactory($container))->create();
    },
    ResultAction\Output\ToConsole\OutputToConsole::class => function (ContainerInterface $container) {
        return (new ResultAction\Output\ToConsole\OutputToConsoleFactory($container))->create();
    },
    'file_output.csv' => function (ContainerInterface $container) {
        return (new ResultAction\Output\ToFile\OutputToCsvFactory($container))->create();
    },
    'file_output.txt' => function (ContainerInterface $container) {
        return (new ResultAction\Output\ToFile\OutputToTxtFactory($container))->create();
    },
    ResultAction\Output\ToFile\TypeAdapter\Csv\CsvAdapter::class => function (ContainerInterface $container) {
        return (new ResultAction\Output\ToFile\TypeAdapter\Csv\CsvAdapterFactory($container))->create();
    },
    ResultAction\Output\ToFile\TypeAdapter\Txt\TxtAdapter::class => function (ContainerInterface $container) {
        return (new ResultAction\Output\ToFile\TypeAdapter\Txt\TxtAdapterFactory($container))->create();
    },
    \League\Flysystem\Filesystem::class => function () {
        return new \League\Flysystem\Filesystem(
            new \League\Flysystem\Adapter\Local(__DIR__ . '/../')
        );
    },
    \League\Flysystem\FilesystemInterface::class => \DI\get(\League\Flysystem\Filesystem::class),
    \GuzzleHttp\ClientInterface::class => \DI\get(\GuzzleHttp\Client::class),
    \GuzzleHttp\Client::class => function (): \GuzzleHttp\Client {
        $middleware = new \BenTools\GuzzleHttp\Middleware\DurationHeaderMiddleware('X-Request-Duration');
        $client = new \GuzzleHttp\Client();
        $client->getConfig('handler')->push($middleware);

        return $client;
    },
];

