<?php declare(strict_types=1);

namespace GGajda\PageSpeed\Command;

use GGajda\PageSpeed\Benchmark\PageSpeed as BenchmarkRunner;
use GGajda\PageSpeed\Benchmark\TestResult;
use GGajda\PageSpeed\ResultAction\CommandResult;
use GGajda\PageSpeed\ResultAction\Output\ToFile\OutputToCsvFactory;
use GGajda\PageSpeed\ResultAction\Output\ToFile\OutputToTxtFactory;
use GuzzleHttp\Psr7\Uri;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Style\SymfonyStyle;

class PageSpeed extends Command
{
    public const COMMAND_NAME = 'pagespeed';
    private const MAIN_URL_DESC = 'Main url to test performance';
    private const TEST_URL_DESC = 'Test url to measure speed of main website';
    private const TXT_FILENAME_DESC = 'Filename for your results in txt format';
    private const CSV_FILENAME_DESC = 'Filename for your results in csv format';

    private $pagespeed;
    private $actions;

    public function __construct(BenchmarkRunner $pageSpeed, CommandResult ...$actions)
    {
        parent::__construct(self::COMMAND_NAME);
        $this->pagespeed = $pageSpeed;
        $this->actions = $actions;
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $consoleOutput = new SymfonyStyle($input, $output);
        $consoleOutput->title(' Pagespeed ');
        $consoleOutput->text([
            'Simple CLI application written in PHP to benchmark',
            'response time one or more websites and compare results',
            'to first input.'
        ]);

        $mainUrl = new Uri($input->getArgument('main_url'));
        $testUrl = \array_map(function (string $url): Uri {
            return new Uri($url);
        }, $input->getArgument('test_url'));

        if (empty($this->actions) || empty($testUrl)) {
            $consoleOutput->warning('Application has not defined any actions or list of urls to test is empty.');

            return 1;
        }

        try {
            foreach ($this->actions as $action) {
                $action->beforeCommand($input, $output);
            }

            $actions = $this->handleActions($input, $output);
            foreach ($this->pagespeed->runBenchmark($mainUrl, ...$testUrl) as $result) {
                $actions($result);
            }

            foreach ($this->actions as $action) {
                $action->afterCommand($input, $output);
            }

            return 0;
        } catch (\Exception $e) {
            $consoleOutput->error($e->getMessage());

            return 2;
        }
    }

    protected function configure()
    {
        $this
            ->addUsage("is simple CLI application to benchmark response time\n  one or more websites and compare results to first input.")
            ->addArgument(
                'main_url',
                InputArgument::REQUIRED,
                self::MAIN_URL_DESC
            )
            ->addArgument(
                'test_url',
                InputArgument::IS_ARRAY,
                self::TEST_URL_DESC,
                []
            )
            ->addOption(
                OutputToTxtFactory::OPTION_NAME,
                '',
                InputOption::VALUE_OPTIONAL,
                self::TXT_FILENAME_DESC,
                false
            )
            ->addOption(
                OutputToCsvFactory::OPTIONE_NAME,
                '',
                InputOption::VALUE_OPTIONAL,
                self::CSV_FILENAME_DESC,
                false
            );
    }

    private function handleActions(InputInterface $input, OutputInterface $output): callable {
        return function (TestResult $result) use ($input, $output): void {
            foreach ($this->actions as $action) {
                $action->handleResult($result, $input, $output);
            }
        };
    }
}
