<?php declare(strict_types=1);

namespace GGajda\PageSpeed\Command;

use GGajda\PageSpeed\ResultAction\Output\ToConsole\OutputToConsole;
use Psr\Container\ContainerInterface;

class PageSpeedFactory
{
    private $container;

    public function __construct(ContainerInterface $container)
    {
        $this->container = $container;
    }

    public function create(): PageSpeed
    {
        return new PageSpeed(
            $this->container->get(\GGajda\PageSpeed\Benchmark\PageSpeed::class),
            $this->container->get(OutputToConsole::class),
            $this->container->get('file_output.txt'),
            $this->container->get('file_output.csv')
        );
    }
}
