<?php declare(strict_types=1);

namespace GGajda\PageSpeed\Benchmark;

use GGajda\PageSpeed\Benchmark\Exception\EmptyTestUrlDatasetException;
use Psr\Http\Message\UriInterface;

class PageSpeed
{
    private $testRunner;

    public function __construct(TestRunner $testRunner)
    {
        $this->testRunner = $testRunner;
    }

    public function runBenchmark(UriInterface $mainUrl, UriInterface ...$testUrls): iterable
    {
        $this->isEmptyDataset(...$testUrls);

        foreach ($testUrls as $testUrl) {
            yield $this->testRunner->runTest($mainUrl, $testUrl);
        }
    }

    private function isEmptyDataset(UriInterface ...$testUrls): void
    {
        if (\count($testUrls) === 0) {
            throw new EmptyTestUrlDatasetException('Passed 0 urls to test pagespeed');
        }
    }
}
