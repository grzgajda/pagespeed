<?php declare(strict_types=1);

namespace GGajda\PageSpeed\Benchmark\Factory;

use GGajda\PageSpeed\Benchmark\TestRunner;
use GuzzleHttp\ClientInterface;
use Psr\Container\ContainerInterface;

class HttpTestRunnerFactory
{
    private $container;

    public function __construct(ContainerInterface $container)
    {
        $this->container = $container;
    }

    public function create(): TestRunner
    {
        return new \GGajda\PageSpeed\Benchmark\Http\HttpTestRunner(
            $this->container->get(ClientInterface::class)
        );
    }
}
