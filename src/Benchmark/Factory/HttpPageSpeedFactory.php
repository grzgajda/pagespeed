<?php declare(strict_types=1);

namespace GGajda\PageSpeed\Benchmark\Factory;

use GGajda\PageSpeed\Benchmark\PageSpeed;
use GGajda\PageSpeed\Benchmark\TestRunner;
use Psr\Container\ContainerInterface;

class HttpPageSpeedFactory
{
    private $container;

    public function __construct(ContainerInterface $container)
    {
        $this->container = $container;
    }

    public function create(): PageSpeed
    {
        return new PageSpeed($this->container->get(TestRunner::class));
    }
}
