<?php declare(strict_types=1);

namespace GGajda\PageSpeed\Benchmark;

use Psr\Http\Message\UriInterface;

interface TestRunner
{
    public function runTest(UriInterface $mainUrl, UriInterface $testUrl): TestResult;
}
