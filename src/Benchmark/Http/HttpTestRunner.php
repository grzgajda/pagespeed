<?php declare(strict_types=1);

namespace GGajda\PageSpeed\Benchmark\Http;

use GGajda\PageSpeed\Benchmark\Exception\PageNotFoundException;
use GGajda\PageSpeed\Benchmark\Exception\ServerErrorException;
use GGajda\PageSpeed\Benchmark\Exception\TooManyRedirectsException;
use GGajda\PageSpeed\Benchmark\TestResult;
use GGajda\PageSpeed\Benchmark\TestRunner;
use GuzzleHttp\ClientInterface;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\UriInterface;

class HttpTestRunner implements TestRunner
{
    public const REQUEST_METHOD = 'GET';
    private const REQUEST_OPTIONS = [
        'http_errors' => false
    ];

    private $httpClient;

    public function __construct(ClientInterface $httpClient)
    {
        $this->httpClient = $httpClient;
    }

    public function runTest(UriInterface $mainUrl, UriInterface $testUrl): TestResult
    {
        if ($mainUrl === $testUrl) {
            return new HttpTestResult(0, 0, $mainUrl, $testUrl);
        }

        $mainResponse = $this->makeRequest($mainUrl);
        $testResponse = $this->makeRequest($testUrl);

        return new HttpTestResult(
            $this->getRequestDuration($mainResponse),
            $this->getRequestDuration($testResponse),
            $mainUrl,
            $testUrl
        );
    }

    private function getRequestDuration(ResponseInterface $response): float {
        return (float) $response->getHeaderLine('X-Request-Duration');
    }

    private function makeRequest(UriInterface $uri): ResponseInterface
    {
        $response = $this->httpClient->request(self::REQUEST_METHOD, $uri, self::REQUEST_OPTIONS);
        $this->tooManyRedirectsException($uri, $response);
        $this->pageNotFoundException($uri, $response);
        $this->serverErrorException($uri, $response);

        return $response;
    }

    private function tooManyRedirectsException(UriInterface $uri, ResponseInterface $response): void
    {
        $message = \sprintf('Page %s has too many redirects, returned with status %d', $uri, $response->getStatusCode());

        if ($response->getStatusCode() >= 300 && $response->getStatusCode() < 400) {
            throw new TooManyRedirectsException($message);
        }
    }

    private function pageNotFoundException(UriInterface $uri, ResponseInterface $response): void
    {
        $message = \sprintf('Page %s not found, returned with status %d', $uri, $response->getStatusCode());

        if ($response->getStatusCode() >= 400 && $response->getStatusCode() < 500) {
            throw new PageNotFoundException($message);
        }
    }

    private function serverErrorException(UriInterface $uri, ResponseInterface $response): void
    {
        $message = \sprintf('Page %s is not available, returned with status %d', $uri, $response->getStatusCode());

        if ($response->getStatusCode() >= 500 && $response->getStatusCode() < 600) {
            throw new ServerErrorException($message);
        }
    }
}
