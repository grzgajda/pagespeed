<?php declare(strict_types=1);

namespace GGajda\PageSpeed\Benchmark\Http;

use GGajda\PageSpeed\Benchmark\TestResult;
use Psr\Http\Message\UriInterface;

class HttpTestResult implements TestResult
{
    private $mainRequestDuration;
    private $testRequestDuration;
    private $mainUrl;
    private $testUrl;

    public function __construct(
        float $mainRequestDuration,
        float $testRequestDuration,
        UriInterface $mainUrl,
        UriInterface $testUrl
    )
    {
        $this->mainRequestDuration = $mainRequestDuration;
        $this->testRequestDuration = $testRequestDuration;
        $this->mainUrl = $mainUrl;
        $this->testUrl = $testUrl;
    }

    public function getMainUrl(): UriInterface
    {
        return $this->mainUrl;
    }

    public function getTestUrl(): UriInterface
    {
        return $this->testUrl;
    }

    public function isFaster(): bool
    {
        return $this->mainRequestDuration < $this->testRequestDuration;
    }

    public function isSlower(): bool
    {
        return $this->mainRequestDuration >= $this->testRequestDuration;
    }

    public function getMainDuration(): float
    {
        return $this->mainRequestDuration;
    }

    public function getTestDuration(): float
    {
        return $this->testRequestDuration;
    }
}
