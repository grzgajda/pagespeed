<?php declare(strict_types=1);

namespace GGajda\PageSpeed\Benchmark;

use Psr\Http\Message\UriInterface;

interface TestResult
{
    public function getMainUrl(): UriInterface;

    public function getTestUrl(): UriInterface;

    public function isFaster(): bool;

    public function isSlower(): bool;

    public function getMainDuration(): float;

    public function getTestDuration(): float;
}
