<?php declare(strict_types=1);

namespace GGajda\PageSpeed\Benchmark\Exception;

class EmptyTestUrlDatasetException extends \InvalidArgumentException
{
}
