<?php declare(strict_types=1);

namespace GGajda\PageSpeed\Benchmark\Exception;

class TooManyRedirectsException extends \RuntimeException
{
}
