<?php declare(strict_types=1);

namespace GGajda\PageSpeed\ResultAction;

use GGajda\PageSpeed\Benchmark\TestResult;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

interface CommandResult
{
    public function beforeCommand(InputInterface $input, OutputInterface $output): void;

    public function handleResult(TestResult $result, InputInterface $input, OutputInterface $output): void;

    public function afterCommand(InputInterface $input, OutputInterface $output): void;
}
