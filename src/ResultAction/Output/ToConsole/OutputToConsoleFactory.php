<?php declare(strict_types=1);

namespace GGajda\PageSpeed\ResultAction\Output\ToConsole;

class OutputToConsoleFactory
{
    public function create(): OutputToConsole
    {
        return new OutputToConsole();
    }
}
