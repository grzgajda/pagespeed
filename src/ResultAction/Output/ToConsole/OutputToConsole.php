<?php declare(strict_types=1);

namespace GGajda\PageSpeed\ResultAction\Output\ToConsole;

use GGajda\PageSpeed\Benchmark\TestResult;
use GGajda\PageSpeed\ResultAction\CommandResult;
use Symfony\Component\Console\Helper\Table;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\ConsoleOutput;
use Symfony\Component\Console\Output\OutputInterface;

class OutputToConsole implements CommandResult
{
    private $table;

    public function beforeCommand(InputInterface $input, OutputInterface $output): void
    {
        $this->isPossibleToCreateSection($output);

        /** @var ConsoleOutput $output */
        $section = $output->section();
        $this->table = new Table($section);
        $this->table->setHeaders(['Main URL', 'Test URL', 'Is faster?', 'Main duration', 'Test duration']);
    }

    public function handleResult(TestResult $result, InputInterface $input, OutputInterface $output): void
    {
        $this->isTableInitialized();

        $this->table->appendRow([
            $result->getMainUrl(),
            $result->getTestUrl(),
            $this->isFaster($result->isFaster()),
            $result->getMainDuration(),
            $result->getTestDuration()
        ]);
    }

    public function afterCommand(InputInterface $input, OutputInterface $output): void
    {
    }

    private function isFaster(bool $isFaster): string
    {
        return $isFaster ? 'Yes' : 'No';
    }

    private function isPossibleToCreateSection(OutputInterface $output): void
    {
        if (false === $output instanceof ConsoleOutput || false === method_exists($output, 'section')) {
            throw new \InvalidArgumentException('To run action "OutputToConsole", output must have a possibility to create section.');
        }
    }

    private function isTableInitialized(): void
    {
        if (null === $this->table) {
            throw new \RuntimeException('You need to execute "beforeCommand" method first!');
        }
    }
}
