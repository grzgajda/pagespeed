<?php declare(strict_types=1);

namespace GGajda\PageSpeed\ResultAction\Output\ToFile;

use GGajda\PageSpeed\ResultAction\Output\ToFile\TypeAdapter\Txt\TxtAdapter;
use League\Flysystem\FilesystemInterface;
use Psr\Container\ContainerInterface;

class OutputToTxtFactory
{
    public const OPTION_NAME = 'with-txt';

    private $container;

    public function __construct(ContainerInterface $container)
    {
        $this->container = $container;
    }

    public function create(): OutputToFile
    {
        return new OutputToFile(
            $this->container->get(FilesystemInterface::class),
            $this->container->get(TxtAdapter::class),
            self::OPTION_NAME
        );
    }
}
