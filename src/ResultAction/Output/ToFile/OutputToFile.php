<?php declare(strict_types=1);

namespace GGajda\PageSpeed\ResultAction\Output\ToFile;

use GGajda\PageSpeed\Benchmark\TestResult;
use GGajda\PageSpeed\ResultAction\CommandResult;
use League\Flysystem\FilesystemInterface;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class OutputToFile implements CommandResult
{
    private $filesystem;
    private $adapter;
    private $optionName;

    public function __construct(FilesystemInterface $filesystem, FileTypeAdapter $adapter, string $optionName)
    {
        $this->filesystem = $filesystem;
        $this->adapter = $adapter;
        $this->optionName = $optionName;
    }

    public function beforeCommand(InputInterface $input, OutputInterface $output): void
    {
        if (false === $this->isOptionEnabled($input)) {
            return;
        }

        $filename = $input->getOption($this->optionName);
        $this->filesystem->write($filename, $this->adapter->getHeader() . PHP_EOL);
    }

    public function handleResult(TestResult $result, InputInterface $input, OutputInterface $output): void
    {
        if (false === $this->isOptionEnabled($input)) {
            return;
        }

        $filename = $input->getOption($this->optionName);
        $oldContent = $this->filesystem->read($filename);
        $newContent = $this->adapter->getRow($result);

        $this->filesystem->put(
            $filename,
            \sprintf("%s%s\n", $oldContent, $newContent)
        );
    }

    public function afterCommand(InputInterface $input, OutputInterface $output): void
    {
    }

    private function isOptionEnabled(InputInterface $input): bool
    {
        return $input->hasOption($this->optionName)
            && \is_string($input->getOption($this->optionName))
            && !empty($input->getOption($this->optionName));
    }
}
