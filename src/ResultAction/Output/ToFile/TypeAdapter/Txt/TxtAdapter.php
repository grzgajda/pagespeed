<?php declare(strict_types=1);

namespace GGajda\PageSpeed\ResultAction\Output\ToFile\TypeAdapter\Txt;

use GGajda\PageSpeed\Benchmark\TestResult;
use GGajda\PageSpeed\ResultAction\Output\ToFile\FileTypeAdapter;

class TxtAdapter implements FileTypeAdapter
{
    public function getHeader(): string
    {
        return 'Main URL | Test URL | Is faster? | Main duration | Test duration';
    }

    public function getRow(TestResult $result): string
    {
        return implode(' | ', [
            $result->getMainUrl(),
            $result->getTestUrl(),
            $result->isFaster() ? 'Yes' : 'No',
            $result->getMainDuration(),
            $result->getTestDuration()
        ]);
    }
}
