<?php declare(strict_types=1);

namespace GGajda\PageSpeed\ResultAction\Output\ToFile\TypeAdapter\Txt;

class TxtAdapterFactory
{
    public function create(): TxtAdapter
    {
        return new TxtAdapter();
    }
}
