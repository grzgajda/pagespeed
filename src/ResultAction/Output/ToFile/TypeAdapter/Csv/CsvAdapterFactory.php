<?php declare(strict_types=1);

namespace GGajda\PageSpeed\ResultAction\Output\ToFile\TypeAdapter\Csv;

class CsvAdapterFactory
{
    public function create(): CsvAdapter
    {
        return new CsvAdapter();
    }
}
