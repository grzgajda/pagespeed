<?php declare(strict_types=1);

namespace GGajda\PageSpeed\ResultAction\Output\ToFile;

use GGajda\PageSpeed\ResultAction\Output\ToFile\TypeAdapter\Csv\CsvAdapter;
use League\Flysystem\FilesystemInterface;
use Psr\Container\ContainerInterface;

class OutputToCsvFactory
{
    public const OPTIONE_NAME = 'with-csv';

    private $container;

    public function __construct(ContainerInterface $container)
    {
        $this->container = $container;
    }

    public function create(): OutputToFile
    {
        return new OutputToFile(
            $this->container->get(FilesystemInterface::class),
            $this->container->get(CsvAdapter::class),
            self::OPTIONE_NAME
        );
    }
}
