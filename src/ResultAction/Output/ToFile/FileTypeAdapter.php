<?php declare(strict_types=1);

namespace GGajda\PageSpeed\ResultAction\Output\ToFile;

use GGajda\PageSpeed\Benchmark\TestResult;

interface FileTypeAdapter
{
    public function getHeader(): string;

    public function getRow(TestResult $result): string;
}
