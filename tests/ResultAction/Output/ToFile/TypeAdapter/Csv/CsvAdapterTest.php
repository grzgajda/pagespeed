<?php declare(strict_types=1);

namespace GGajda\PageSpeedTests\ResultAction\Output\ToFile\TypeAdapter\Csv;

use GGajda\PageSpeed\Benchmark\TestResult;
use GGajda\PageSpeed\ResultAction\Output\ToFile\TypeAdapter\Csv\CsvAdapter;
use PHPUnit\Framework\TestCase;

class CsvAdapterTest extends TestCase
{
    public function testBuildFile(): void
    {
        $result = $this->createMock(TestResult::class);

        $adapter = new CsvAdapter();
        $header = $adapter->getHeader();
        $row = $adapter->getRow($result);

        $this->assertNotEmpty($header);
        $this->assertNotEmpty($row);
    }
}
