<?php declare(strict_types=1);

namespace GGajda\PageSpeedTests\ResultAction\Output\ToFile\TypeAdapter\Txt;

use GGajda\PageSpeed\Benchmark\TestResult;
use GGajda\PageSpeed\ResultAction\Output\ToFile\TypeAdapter\Txt\TxtAdapter;
use PHPUnit\Framework\TestCase;

class TxtAdapterTest extends TestCase
{
    public function testBuildFile(): void
    {
        $result = $this->createMock(TestResult::class);

        $adapter = new TxtAdapter();
        $header = $adapter->getHeader();
        $row = $adapter->getRow($result);

        $this->assertNotEmpty($header);
        $this->assertNotEmpty($row);
    }
}
