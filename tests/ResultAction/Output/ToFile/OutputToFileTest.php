<?php declare(strict_types=1);

namespace GGajda\PageSpeedTests\ResultAction\Output\ToFile;

use GGajda\PageSpeed\Benchmark\TestResult;
use GGajda\PageSpeed\ResultAction\Output\ToFile\FileTypeAdapter;
use GGajda\PageSpeed\ResultAction\Output\ToFile\OutputToFile;
use League\Flysystem\FilesystemInterface;
use PHPUnit\Framework\TestCase;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class OutputToFileTest extends TestCase
{
    public function testNothingWhenOptionsNotExists(): void
    {
        $this->expectNotToPerformAssertions();
        $filesystem = $this->createMock(FilesystemInterface::class);
        $adapter = $this->createMock(FileTypeAdapter::class);

        $input = $this->createMock(InputInterface::class);
        $input->method('hasOption')->willReturn(false);

        $output = $this->createMock(OutputInterface::class);
        $result = $this->createMock(TestResult::class);

        $action = new OutputToFile($filesystem, $adapter, 'with-txt');
        $action->beforeCommand($input, $output);
        $action->handleResult($result, $input, $output);
    }

    public function testNothingWhenOptionNotString(): void
    {
        $this->expectNotToPerformAssertions();
        $filesystem = $this->createMock(FilesystemInterface::class);
        $adapter = $this->createMock(FileTypeAdapter::class);

        $input = $this->createMock(InputInterface::class);
        $input->method('hasOption')->willReturn(true);
        $input->method('getOption')->willReturn(false);

        $output = $this->createMock(OutputInterface::class);
        $result = $this->createMock(TestResult::class);

        $action = new OutputToFile($filesystem, $adapter, 'with-txt');
        $action->beforeCommand($input, $output);
        $action->handleResult($result, $input, $output);
    }

    public function testNothingWhenOptionEmptyString(): void
    {
        $this->expectNotToPerformAssertions();
        $filesystem = $this->createMock(FilesystemInterface::class);
        $adapter = $this->createMock(FileTypeAdapter::class);

        $input = $this->createMock(InputInterface::class);
        $input->method('hasOption')->willReturn(true);
        $input->method('getOption')->willReturn('');

        $output = $this->createMock(OutputInterface::class);
        $result = $this->createMock(TestResult::class);

        $action = new OutputToFile($filesystem, $adapter, 'with-txt');
        $action->beforeCommand($input, $output);
        $action->handleResult($result, $input, $output);
    }

    public function testWriteToFile(): void
    {
        $filesystem = $this->createMock(FilesystemInterface::class);
        $filesystem->expects($this->once())->method('write')->willReturn(true);
        $filesystem->expects($this->once())->method('put')->willReturn(true);

        $adapter = $this->createMock(FileTypeAdapter::class);
        $adapter->expects($this->once())->method('getHeader')->willReturn('1 | 2');
        $adapter->expects($this->once())->method('getRow')->willReturn('2 | 1');

        $input = $this->createMock(InputInterface::class);
        $input->method('hasOption')->willReturn(true);
        $input->method('getOption')->willReturn('text.csv');

        $output = $this->createMock(OutputInterface::class);
        $result = $this->createMock(TestResult::class);

        $action = new OutputToFile($filesystem, $adapter, 'with-txt');
        $action->beforeCommand($input, $output);
        $action->handleResult($result, $input, $output);
    }

    public function testDoNothingAfterCommand(): void
    {
        $this->expectNotToPerformAssertions();

        $filesystem = $this->createMock(FilesystemInterface::class);
        $adapter = $this->createMock(FileTypeAdapter::class);
        $input = $this->createMock(InputInterface::class);
        $output = $this->createMock(OutputInterface::class);

        $action = new OutputToFile($filesystem, $adapter, 'with-txt');
        $action->afterCommand($input, $output);
    }
}
