<?php declare(strict_types=1);

namespace GGajda\PageSpeedTests\Benchmark\Http;

use GGajda\PageSpeed\Benchmark\Http\HttpTestResult;
use GuzzleHttp\Psr7\Uri;
use PHPUnit\Framework\TestCase;

class HttpTestResultTest extends TestCase
{
    public function testIsFaster(): void
    {
        $mainUrl = new Uri('https://devmint.pl');
        $testUrl = new Uri('https://alerabat.com');

        $testResult = new HttpTestResult(200, 300, $mainUrl, $testUrl);
        $this->assertTrue($testResult->isFaster());
        $this->assertFalse($testResult->isSlower());
    }

    public function testIsSlower(): void
    {
        $mainUrl = new Uri('https://devmint.pl');
        $testUrl = new Uri('https://alerabat.com');

        $testResult = new HttpTestResult(300, 200, $mainUrl, $testUrl);
        $this->assertFalse($testResult->isFaster());
        $this->assertTrue($testResult->isSlower());
    }

    public function testIsSlowerSameResult(): void
    {
        $mainUrl = new Uri('https://devmint.pl');
        $testUrl = new Uri('https://alerabat.com');

        $testResult = new HttpTestResult(300, 300, $mainUrl, $testUrl);
        $this->assertFalse($testResult->isFaster());
        $this->assertTrue($testResult->isSlower());
    }
}
