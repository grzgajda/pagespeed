<?php declare(strict_types=1);

namespace GGajda\PageSpeedTests\Benchmark\Http;

use GGajda\PageSpeed\Benchmark\Exception\PageNotFoundException;
use GGajda\PageSpeed\Benchmark\Exception\ServerErrorException;
use GGajda\PageSpeed\Benchmark\Exception\TooManyRedirectsException;
use GGajda\PageSpeed\Benchmark\Http\HttpTestRunner;
use GuzzleHttp\ClientInterface;
use GuzzleHttp\Psr7\Uri;
use PHPUnit\Framework\TestCase;
use Psr\Http\Message\ResponseInterface;

class HttpTestRunnerTest extends TestCase
{
    public function testCreateResult(): void
    {
        $mainUrl = new Uri('https://devmint.pl');
        $testUrl = new Uri('https://alerabat.com');

        $httpClient = $this->createMock(ClientInterface::class);
        $httpClient->method('request')
            ->willReturnCallback(function (string $method, Uri $uri) use ($mainUrl) {
                $durationTime = $uri === $mainUrl
                    ? '234.23'
                    : '323';

                $response = $this->createMock(ResponseInterface::class);
                $response->method('getHeaderLine')->willReturn($durationTime);

                return $response;
            });

        $testRunner = new HttpTestRunner($httpClient);
        $testResult = $testRunner->runTest($mainUrl, $testUrl);

        $this->assertTrue($testResult->isFaster());
        $this->assertFalse($testResult->isSlower());
    }

    public function testDoesNotRequestWhenUrlAreTheSame(): void
    {
        $mainUrl = $testUrl = new Uri('https://devmint.pl');

        $httpClient = $this->createMock(ClientInterface::class);

        $testRunner = new HttpTestRunner($httpClient);
        $testResult = $testRunner->runTest($mainUrl, $testUrl);

        $this->assertTrue($testResult->isSlower());
    }

    public function testStatusCode300(): void
    {
        $this->expectException(TooManyRedirectsException::class);
        $mainUrl = new Uri('https://devmint.pl');
        $testUrl = new Uri('https://alerabat.com');

        $httpClient = $this->createMock(ClientInterface::class);
        $httpClient->expects($this->once())->method('request')->willReturnCallback(function () {
            $response = $this->createMock(ResponseInterface::class);
            $response->method('getStatusCode')->willReturn(301);

            return $response;
        });

        $testRunner = new HttpTestRunner($httpClient);
        $testRunner->runTest($mainUrl, $testUrl);
    }

    public function testStatusCode400(): void
    {
        $this->expectException(PageNotFoundException::class);
        $mainUrl = new Uri('https://devmint.pl');
        $testUrl = new Uri('https://alerabat.com');

        $httpClient = $this->createMock(ClientInterface::class);
        $httpClient->expects($this->once())->method('request')->willReturnCallback(function () {
            $response = $this->createMock(ResponseInterface::class);
            $response->method('getStatusCode')->willReturn(404);

            return $response;
        });

        $testRunner = new HttpTestRunner($httpClient);
        $testRunner->runTest($mainUrl, $testUrl);
    }

    public function testStatusCode500(): void
    {
        $this->expectException(ServerErrorException::class);
        $mainUrl = new Uri('https://devmint.pl');
        $testUrl = new Uri('https://alerabat.com');

        $httpClient = $this->createMock(ClientInterface::class);
        $httpClient->expects($this->once())->method('request')->willReturnCallback(function () {
            $response = $this->createMock(ResponseInterface::class);
            $response->method('getStatusCode')->willReturn(500);

            return $response;
        });

        $testRunner = new HttpTestRunner($httpClient);
        $testRunner->runTest($mainUrl, $testUrl);
    }
}
