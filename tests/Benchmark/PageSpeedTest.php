<?php declare(strict_types=1);

namespace GGajda\PageSpeedTests\Benchmark;

use GGajda\PageSpeed\Benchmark\Exception\EmptyTestUrlDatasetException;
use GGajda\PageSpeed\Benchmark\PageSpeed;
use GGajda\PageSpeed\Benchmark\TestResult;
use GGajda\PageSpeed\Benchmark\TestRunner;
use GuzzleHttp\Psr7\Uri;
use PHPUnit\Framework\TestCase;

class PageSpeedTest extends TestCase
{
    public function testExceptionWhenNotFoundTestUrls(): void
    {
        $this->expectException(EmptyTestUrlDatasetException::class);

        $testRunner = $this->createMock(TestRunner::class);
        $pagespeed = new PageSpeed($testRunner);
        $pagespeed->runBenchmark(new Uri('https://devmint.pl'))->next();
    }

    public function testIterateResultsForOneUrl(): void
    {
        $mainUrl = new Uri('https://devmint.pl');
        $testUrl = new Uri('https://alerabat.com');

        $testRunner = $this->createMock(TestRunner::class);
        $testRunner->method('runTest')->with($mainUrl, $testUrl)->willReturnCallback(function () {
            $result = $this->createMock(TestResult::class);
            $result->method('isFaster')->willReturn(true);
            $result->method('isSlower')->willReturn(false);

            return $result;
        });

        $pagespeed = new PageSpeed($testRunner);
        foreach ($pagespeed->runBenchmark($mainUrl, $testUrl) as $result) {
            $this->assertTrue($result->isFaster());
            $this->assertFalse($result->isSlower());
        }
    }
}
