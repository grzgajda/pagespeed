<?php declare(strict_types=1);

namespace GGajda\PageSpeedTests\Command;

use GGajda\PageSpeed\Benchmark\TestResult;
use GGajda\PageSpeed\Command\PageSpeed;
use GGajda\PageSpeed\ResultAction\CommandResult;
use PHPUnit\Framework\TestCase;
use Symfony\Component\Console\Tester\CommandTester;

class PageSpeedTest extends TestCase
{
    public function testNoActions(): void
    {
        $runner = $this->createMock(\GGajda\PageSpeed\Benchmark\PageSpeed::class);

        $pagespeed = new PageSpeed($runner);
        $command = new CommandTester($pagespeed);
        $command->execute([
            'main_url' => 'https://devmint.pl',
            'test_url' => ['https://alerabat.com', 'https://onet.pl']
        ]);

        $this->assertEquals(1, $command->getStatusCode());
        $this->assertContains('WARNING', $command->getDisplay());
    }

    public function testNoTestUrls(): void
    {
        $action = $this->createMock(CommandResult::class);
        $runner = $this->createMock(\GGajda\PageSpeed\Benchmark\PageSpeed::class);

        $pagespeed = new PageSpeed($runner, $action);
        $command = new CommandTester($pagespeed);
        $command->execute([
            'main_url' => 'https://devmint.pl',
        ]);

        $this->assertEquals(1, $command->getStatusCode());
        $this->assertContains('WARNING', $command->getDisplay());
    }

    public function testRunActionOnce(): void
    {
        $action = $this->createMock(CommandResult::class);
        $action->expects($this->once())->method('beforeCommand');
        $action->expects($this->exactly(2))->method('handleResult');
        $action->expects($this->once())->method('afterCommand');

        $runner = $this->createMock(\GGajda\PageSpeed\Benchmark\PageSpeed::class);
        $runner->expects($this->once())->method('runBenchmark')->willReturnCallback(function () {
            yield $this->createMock(TestResult::class);
            yield $this->createMock(TestResult::class);
        });

        $pagespeed = new PageSpeed($runner, $action);
        $command = new CommandTester($pagespeed);
        $command->execute([
            'main_url' => 'https://devmint.pl',
            'test_url' => ['https://alerabat.com', 'https://onet.pl']
        ]);

        $this->assertEquals(0, $command->getStatusCode());
    }

    public function testRunActionException(): void
    {
        $action = $this->createMock(CommandResult::class);
        $action->expects($this->once())
            ->method('beforeCommand')
            ->willThrowException(new \RuntimeException('Lorem ipsum'));

        $runner = $this->createMock(\GGajda\PageSpeed\Benchmark\PageSpeed::class);

        $pagespeed = new PageSpeed($runner, $action);
        $command = new CommandTester($pagespeed);
        $command->execute([
            'main_url' => 'https://devmint.pl',
            'test_url' => ['https://alerabat.com', 'https://onet.pl']
        ]);

        $this->assertEquals(2, $command->getStatusCode());
        $this->assertContains('Lorem ipsum', $command->getDisplay());
    }
}
